#!/usr/bin/env python3
"""
A simple script for converting UDP unicast to Ethernet broadcast and vice-versa
"""
import socket
import threading
import time
import binascii
import paho.mqtt.client as mqtt
import signal
import logging.handlers
import random
import string
import queue
import sys
import datetime

""" CONFIGURATION PARAMETERS """

""" MQTT Broker address, this is usually the address of the OEM Server """
MQTT_BROKER_ADDRESS = "80.255.245.187"
# MQTT_BROKER_ADDRESS = "192.168.2.11"

""" Incoming Ethernet II frame configuration """
ETH_II_BUF_SIZE = 1600  # > 1500
ETH_P_ALL = 3  # To receive all Ethernet protocols

""" Logging parameters """
MAX_LOG_SIZE = 100  # Maximum size of a single log file (in MB)
BACKUP_LOG_COUNT = 5  # Number of rotating log files
CONSOLE_LOG_LEVEL = logging.INFO
""" END OF CONFIGURATION PARAMETERS """


# Packet utility functions
def SMAC(packet):
    return binascii.hexlify(packet[6:12]).decode()


def DMAC(packet):
    return binascii.hexlify(packet[0:6]).decode()


def EtherType(packet):
    return binascii.hexlify(packet[12:14]).decode()


def DecodedPayload(packet):
    return binascii.hexlify(packet[14:]).decode()


def CodedPayload(packet):
    # packet[14:] is the ethernet payload, in out
    return packet[14:]


def printPacket(packet, now, message):
    print(message, "{} bytes at time {}".format(len(packet), now))
    print("SMAC:", SMAC(packet), "DMAC:", DMAC(packet), "Type:", EtherType(packet))
    print("Payload:", DecodedPayload(packet))
    print("-----")


class FormatConverter:
    @staticmethod
    def send_eth_bcast(payload, interface):
        dst = [0xff, 0xff, 0xff, 0xff, 0xff, 0xff]  # Destination: eth broadcast
        src = [0xee, 0xee, 0xee, 0xee, 0xee, 0xee]  # Source: ee:ee:ee:ee:ee:ee
        eth_type = [0x89, 0x47]  # 0x8947 is defined for ETSI GN payload
        FormatConverter.send_eth_packet(src, dst, eth_type, payload, interface)

    @staticmethod
    def send_eth_packet(src, dst, eth_type, payload, interface):
        """Send raw Ethernet packet on interface."""
        assert (len(src) == len(dst) == 6)  # 48-bit ethernet addresses
        assert (len(eth_type) == 2)  # 16-bit ethernet type
        s = socket.socket(socket.AF_PACKET, socket.SOCK_RAW)
        # From the docs: "For raw packet
        # sockets the address is a tuple (ifname, proto [,pkttype [,hatype]])"
        s.bind((interface, 0))
        ethernet = bytearray(dst + src + eth_type)
        # print("Ethernet header = ", ethernet)
        # print("Ethernet payload = ", payload)
        return s.send(ethernet + payload)

    def __init__(self):
        """ BEGIN: Logging setup """
        self.logger = logging.getLogger()
        self.logger.setLevel(logging.DEBUG)
        formatter = logging.Formatter('%(asctime)s [%(levelname)s] %(message)s')

        ch = logging.StreamHandler(sys.stdout)
        ch.setLevel(CONSOLE_LOG_LEVEL)

        info_log_file = "broadcast_converter_" + datetime.datetime.now().strftime("%Y_%m_%d_%H_%M") + ".log"

        # Create file handlers for debug and info logs
        fh_info = logging.handlers.RotatingFileHandler(
            info_log_file,
            maxBytes=MAX_LOG_SIZE * 1024 * 1024,
            backupCount=BACKUP_LOG_COUNT
        )

        fh_info.setLevel(logging.INFO)

        formatter.default_msec_format = '%s.%03d'
        fh_info.setFormatter(formatter)
        ch.setFormatter(formatter)

        # Attach handlers to the self.logger
        self.logger.addHandler(fh_info)
        self.logger.addHandler(ch)
        """ END: Logging setup """

        self.killSignal = False
        self.eth_bcast_queue = queue.Queue()
        self.max_queue_size = 0

    """ Thread Functions """
    def unicastToBroadcast(self):
        random_string = ''.join([random.choice(string.ascii_letters + string.digits) for _n in range(32)])
        print("Starting Unicast to broadcast thread")
        client = mqtt.Client("subscribe_client_" + random_string)

        def on_connect(_client, _userdata, _flags, _rc):
            self.logger.info("MQTT receiver connected!!")
            client.subscribe("ETSI_MESSAGES", qos=0)

        def on_disconnect(_client, _userdata, _rc):
            self.logger.warning("MQTT receiver disconnected")
            self.killSignal = True

        def on_message(_client, _userdata, msg):
            message_string = msg.payload.decode("utf-8")
            self.logger.info(
                "Received {} bytes GN data over MQTT: {}".format(int(len(message_string) / 2), message_string))
            self.eth_bcast_queue.put((message_string, datetime.datetime.now()))
            global max_queue_size
            max_queue_size = max(max_queue_size, self.eth_bcast_queue.qsize())
            self.logger.info("Message queue size = {}, max = {}".format(self.eth_bcast_queue.qsize(), max_queue_size))

        client.on_connect = on_connect
        client.on_message = on_message
        client.on_disconnect = on_disconnect
        assert client.connect(MQTT_BROKER_ADDRESS) == 0
        # client.loop_forever()
        # self.logger.error("Client loop stopped, something went wrong!")
        LOOP_TIMEOUT = 100/1000
        while not self.killSignal:
            self.logger.debug("------ Looping mqtt subscriber")
            # t1 = datetime.datetime.now()
            client.loop(timeout=LOOP_TIMEOUT)
            # t2 = datetime.datetime.now()
            # diff = t2 - t1
            # print("------ Looping took {:.1f} ms".format(1000 * diff.total_seconds()))
        client.disconnect()
        print("Exiting unicast to broadcast thread")

    def broadcastToUnicast(self):
        eth_interface = sys.argv[1]
        random_string = ''.join([random.choice(string.ascii_letters + string.digits) for _n in range(32)])
        self.logger.info("Starting Broadcast to Unicast thread")
        sock = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.htons(ETH_P_ALL))
        sock.bind((eth_interface, ETH_P_ALL))
        # sock.setblocking(1)
        sock.settimeout(5)
        publishClient = mqtt.Client("publish_client_" + random_string)  # create client object
        # client1.on_publish = on_publish  # assign function to callback
        publishClient.connect(MQTT_BROKER_ADDRESS)  # establish connection
        publishClient.loop_start()
        while not self.killSignal:
            now = time.time()
            try:
                self.logger.debug("Waiting for a Broadcast")
                packet = sock.recv(ETH_II_BUF_SIZE)
            except socket.timeout:
                self.logger.debug("Timeout reached")
            else:
                dmac = DMAC(packet)
                smac = SMAC(packet)
                """
                    1) Check if destination mac is 0xffffffffffff, meaning broadcast
                    2) EtherType for ETSI messages is 0x8947, as specified in the standard
                    3) 0xeeeeeeeeeeee is the source MAC for packets sent by this script, 
                       hence they must be filtered out, else it'll lead to infinite looping.
                """
                if smac != "eeeeeeeeeeee" and dmac == "ffffffffffff" and EtherType(packet) == "8947":
                    printPacket(packet, now, "Received over ETH:")
                    self.logger.info("Sending over MQTT: {}".format(DecodedPayload(packet)))
                    (res, _mID) = publishClient.publish("ETSI_MESSAGES", DecodedPayload(packet))  # publish
                    assert res == 0
        publishClient.loop_stop(force=True)
        self.logger.warning("Exiting ETH broadcast to unicast thread")

    def eth_broadcast_worker(self):
        eth_interface = sys.argv[1]
        while not self.killSignal:
            item = self.eth_bcast_queue.get()
            if item is None:
                break
            if item == "stop":
                break
            else:
                message_string, t1 = item
                try:
                    # t1 = datetime.datetime.now()
                    geoNetworkingPdu = binascii.unhexlify(message_string)
                    self.send_eth_bcast(geoNetworkingPdu, eth_interface)
                    t2 = datetime.datetime.now()
                    print("Conversion and sending took {} ms".format(int((t2 - t1).total_seconds() * 1000)))
                    self.logger.info("Sent {} bytes as ETH broadcast: {}".format(
                        len(geoNetworkingPdu), message_string))
                except OSError as os_error:
                    # Problem:  This error is mostly caused by the GN PDU being too big
                    #           and not fitting into one Ethernet frame.
                    # Solution: Make Ethernet MTU of the interface larger.
                    self.logger.error("Error in converting to ETH broadcast: {}".format(os_error))

                finally:
                    self.eth_bcast_queue.task_done()

    def main(self):
        self.logger.info("Starting threads")
        threads = [
            threading.Thread(target=self.unicastToBroadcast),
            threading.Thread(target=self.broadcastToUnicast),
            threading.Thread(target=self.eth_broadcast_worker)
        ]
        print("No of threads: ", len(threads))
        for t in threads:
            t.start()

        """
            1) Checking every 1 second if all threads running.
            2) Stop everything if any thread crashes.
        """
        while True:
            time.sleep(1)
            running = True
            for t in threads:
                running = t.is_alive() and running

            if not running:
                # Ideally all threads must keep working and this section should not
                # be entered without user input of Ctrl+C.
                self.logger.error("Some threads not running, something crashed")
                # In case one or more threads crashed due to some fault,
                # set the kill_signal flag so that all threads exit
                self.killSignal = True
                # This is just to trigger the worker thread so that it can read that
                # the kill_signal flag has been set to true.
                self.eth_bcast_queue.put("stop")
                break


if __name__ == "__main__":
    format_converter = FormatConverter()

    def signal_handler(_sig, _frame):
        print("You pressed Ctrl+C!")
        format_converter.killSignal = True

    signal.signal(signal.SIGINT, signal_handler)
    format_converter.main()
    print("Here at the end of main")
