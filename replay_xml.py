import os
from time import sleep
import socket

""" BEGIN: PARAMETERS """
directory = "logs/"
etsi_app_port = 6000
etsi_app_ip = "192.168.1.2"
""" END: PARAMETERS """


if __name__ == "__main__":
    files = [directory + f for f in os.listdir(directory) if os.path.isfile(directory + f)]
    files = sorted(files)
    print("Read files {}".format(files))
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    while True:
        for file in files:
            sleep(1)
            with open(file, 'r') as file_handler:
                data = file_handler.read()
                sock.sendto(data.encode(), (etsi_app_ip, etsi_app_port))
                print("Data sent")