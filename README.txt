################
Format Converter
################

============
Introduction
============
This piece of software interfaces between OEM Server and ETSI Application.

============
Dependencies
============

An Ubuntu based OS running on any platform (Tested on Raspberry Pi, Orange Pi, PC and Cohda MK5).

* Python3 is needed with pip3
  $ sudo apt-get install python3-pip

* Following pip3 packages are needed
  $ sudo pip3 install paho-mqtt

==========
How To Use
==========

$ sudo python3 broadcast_unicast.py INTERFACE

This INTERFACE is on the same subnet as the MK5 board running the ETSI application.
INTERFACE is usually "eth0", but may depend on installation.


Deployment Scenarios:
---------------------

1) Deployment on MK5 board:
    * Ensure that the physical interface "lo" is used by the conf file of the ETSI Application
      is the same as one used in the broadcast_unicast.py
    * In the .conf file of the ETSI Application make sure the line
        [ ItsG5Interface = eth0 ] is changed to
        [ ItsG5Interface = lo ]

2) Deployment on R-Pi/PC.
    Ensure that the physical interface "eth0" is used by the conf file of the ETSI Application and the
    physical interface used by broadcast_unicast.py are connected to the same router/switch.


Logging Information:
--------------------

1) Logging parameters are at the begingin of the broadcast_unicast.py file.
    * Size of one log file
    * How many rotations to retain

2) Log files are genrated at the same level as the "broadcast_unicast.py" file.


###########
PCAP Replay
###########

============
Introduction
============
This is a script use to play back pcap files containing ETSI Messages (tested with SPAT/MAP/IVI) over the
Ethernet. The transmitted Ethernet frames can then be received by an MK5 board configured to receive ETSI
messages via Ethernet.

============
Dependencies
============

An Ubuntu based OS running on any platform (Tested on Raspberry Pi, Orange Pi, PC and Cohda MK5).

* Python3 is needed with pip3
  $ sudo apt-get install python3-pip

* Following pip3 packages are needed
  $ sudo pip3 install scapy

==========
How To Use
==========

The script uses 2 config parameters, both located at the start of the script.

    * INTERFACE: This refers to the interfce on PC which is connected to the same network as the MK5 Board

    * INTER_PACKET_DELAY:
      Time difference between sending 2 packets in the PCAP file.
      Smaller number means more frequent messages.

$ sudo python3 replay_pcap.py <PCAP file to replay>
