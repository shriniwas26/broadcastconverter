import socket
import os
UDP_IP = "0.0.0.0"
UDP_PORT = 6001

sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP
sock.bind((UDP_IP, UDP_PORT))


directory = "logs/"

i = 0
if not os.path.exists(directory):
    os.mkdir(directory)

while True:
    i += 1
    data, addr = sock.recvfrom(10240) # buffer size is 1024 bytes
    data = data.decode("utf-8")
    print("received message:", data)
    filename = "ETSI_Message_{:02d}.xml".format(i)
    with open(directory + filename, "w") as text_file:
        text_file.write(data)
