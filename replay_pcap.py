import scapy.all as scapy
import sys
from time import sleep

""" Config parameters """
interface = "eth0"
inter_packet_delay = 0.5   # in seconds
""" End: Config parameters"""

fileToReplay = sys.argv[1]
pcapLines = scapy.rdpcap(fileToReplay)

print("Read {} packets".format(len(pcapLines)))

ETSI_ETH_TEMPLATE = scapy.Ether(src="ee:ee:ee:ee:ee:ee", dst="ff:ff:ff:ff:ff:ff", type=0x8947)

for i in range(0, len(pcapLines)):
    print("Sending packet number {:5} Done: {:4.2f} %".format(i+1, 100 * (i+1) / len(pcapLines)))
    p = ETSI_ETH_TEMPLATE /pcapLines[i][scapy.Raw]
    scapy.sendp(p, iface=interface, verbose=False)
    sleep(inter_packet_delay)
